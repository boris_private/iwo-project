import sys
from collections import defaultdict

curseword = [name.strip() for name in open("scheldwoorden.txt").readlines()]

male_total_tweets = 0
male_curse_tweets = 0
female_total_tweets = 0
female_curse_tweets = 0

total_frequency_dict = defaultdict(int)  # A dictionary that will give the most frequently used cursewords
male_frequency_dict = defaultdict(int)  # A dictionary that will give the most frequently used cursewords (by males)
female_frequency_dict = defaultdict(int)  # A dictionary that will give the most frequently used cursewords (by females)


for line in sys.stdin:
    print(male_total_tweets)  # Kept this here so you can see how many (male) tweets the script has processed. Avg is 2 million per month.
    curse = False
    if line.strip().split("\t")[0].strip() == "male":
        male_total_tweets += 1
        for word in line.strip().split("\t")[2].split():
            if word in curseword:
                total_frequency_dict[word] += 1
                male_frequency_dict[word] += 1
                curse = True
        if curse:
            male_curse_tweets += 1

    if line.strip().split("\t")[0].strip() == "female":
        female_total_tweets += 1
        for word in line.strip().split("\t")[2].split():
            if word in curseword:
                total_frequency_dict[word] += 1
                female_frequency_dict[word] += 1
                curse = True
        if curse:
            female_curse_tweets += 1

print("{}\t{}".format("Male total tweets:", male_total_tweets))
print("{}\t{}".format("Male curse tweets:", male_curse_tweets))
print("{}\t{}".format("Female total tweets:", female_total_tweets))
print("{}\t{}".format("Female curse tweets:", female_curse_tweets))

print("\nTotal frequency dict:")
print(total_frequency_dict)

print("\nMale frequency dict:")
print(male_frequency_dict)

print("\nFemale frequency dict:")
print(female_frequency_dict)
