import sys

curseword = [name.strip() for name in open("scheldwoorden.txt").readlines()]

total_tweets = 0
curse_tweets = 0


for line in sys.stdin:
    print("{} / {}".format(curse_tweets, total_tweets))
    total_tweets += 1
    curse = False
    for word in line.strip().split("\t")[1].split():
        if word in curseword:
            curse = True
    if curse:
        curse_tweets += 1


print("{}\t{}".format("total tweets:", total_tweets))
print("{}\t{}".format("curse tweets:", curse_tweets))
