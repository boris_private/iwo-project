#!/bin/bash

# twitter-sample.sh
# This program counts how many tweets are in the sample, how many of those are unique and how many of the unique tweets are retweets.
# It also shows the first 20 unique tweets that are not retweets.
# Boris de Jong
# 12-3-2017

# The tweet collection variable is assigned all tweets on 1 march 2017 at 12:00.
tweet_collection=$(gzip -d < /net/corpora/twitter2/Tweets/2017/03/20170301\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab text)


# Different variables get assigned different information. Total amount of tweets, the amount of unique tweets and the amount of retweets.
total_tweets=$(wc -l <<< "$tweet_collection")
unique_tweets=$(sort -u <<< "$tweet_collection" | wc -l)
retweets=$(sort -u <<< "$tweet_collection" | grep "^RT" | wc -l)

echo "Total tweets: "$total_tweets""
echo "Unique tweets: "$unique_tweets""
echo "Retweets: "$retweets""


# Show the first 20 unique tweets that are not retweets.
grep -v "^RT" <<< "$tweet_collection" | awk '!x[$0]++' | head -n 20